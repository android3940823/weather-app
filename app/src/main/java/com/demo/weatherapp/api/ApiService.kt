package com.demo.weatherapp.api

import com.demo.weatherapp.model.Forecast
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("weather/")
    suspend fun getWeather(
        @Query("q") cityName: String,
        @Query("appid") appid: String,
    ): Response<Forecast>

}