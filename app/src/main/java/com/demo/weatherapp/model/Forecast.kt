package com.demo.weatherapp.model

data class Forecast(
    val name: String? = null,
    val weather: List<Weather>? = null,
    val main: Main? = null,
)

data class Main(
    val temp: String? = null,
    val temp_min: String? = null,
    val temp_max: String? = null,
    val pressure: String? = null,
    val humidity: String? = null,
)

data class Weather(
    val main: String? = null,
    val description: String? = null,
)