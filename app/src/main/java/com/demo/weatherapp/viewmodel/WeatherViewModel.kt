package com.demo.weatherapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo.weatherapp.model.Forecast
import com.demo.weatherapp.repository.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel
@Inject constructor(private val repository: WeatherRepository) : ViewModel() {

    private val _response = MutableLiveData<Forecast>()
    val weatherResponse: LiveData<Forecast>
        get() = _response

    private val _error = MutableLiveData<String>()
    val errorResponse: LiveData<String>
        get() = _error

     fun getWeather(cityName: String) = viewModelScope.launch {

        repository.getWeather(cityName).let { response ->

            if (response.isSuccessful) {
                _response.postValue(response.body())
            } else {
                _error.postValue("Something went wrong")
                Log.d("tag", "getWeather Error: ${response.code()}")
            }
        }
    }
}













