package com.demo.weatherapp

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.demo.weatherapp.databinding.ActivityMainBinding
import com.demo.weatherapp.viewmodel.WeatherViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: WeatherViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Log.e("MainActivity", "MainActivity called")
        viewModel.getWeather("Frisco")

        binding.btnSearch.setOnClickListener {
            if (binding.edtName.text.toString().isEmpty()) {
                Toast.makeText(applicationContext, "Please enter city name", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            viewModel.getWeather(binding.edtName.text.toString())
            hideKeyboard(it)
        }

        viewModel.errorResponse.observe(this) {
            Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
        }
        viewModel.weatherResponse.observe(this) {
            binding.apply {
                tvCityName.text = it?.name ?: "N/a"
                tvTemperature.text = it?.main?.temp ?: "N/a"
                if (it?.weather?.isNotEmpty() == true) {
                    val weather = it.weather[0]
                    tvDescription.text = weather.description ?: "N/a"
                    tvWind.text = weather.description ?: "N/a"
                }
                tvWind.text = it?.main?.humidity ?: "N/a"
            }
        }
    }
    fun hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}