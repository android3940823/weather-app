package com.demo.weatherapp.repository

import com.demo.weatherapp.api.ApiService
import com.demo.weatherapp.utils.Constants
import javax.inject.Inject

class WeatherRepository
@Inject constructor(private val apiService: ApiService) {
    suspend fun getWeather(cityName: String) = apiService.getWeather(cityName, Constants.API_KEY)
}